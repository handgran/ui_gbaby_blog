
$(function(){

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			center:true,
			animateOut: 'fadeOut',
		});
	
	
});
